package ru.ckateptb.permissionmanager.permission.node;

import lombok.RequiredArgsConstructor;
import ru.ckateptb.permissionmanager.tree.StringChildTree;

import java.util.Optional;

@RequiredArgsConstructor
public class StringPermissionNodeChildTree extends StringChildTree<StringPermissionNodeChildTree> {
    protected final PermissionNodeStatus status;

    public void addPermissions(String... permissions) {
        for (String permission : permissions) {
            if (permission == null || permission.trim().isEmpty()) continue;
            PermissionNodeStatus status = PermissionNodeStatus.ALLOW;
            if (permission.startsWith("!")) {
                status = PermissionNodeStatus.DENY;
                permission = permission.substring(1);
            }
            StringPermissionNodeChildTree nodeChildTree = this;
            for (String node : permission.split("\\.")) {
                nodeChildTree = nodeChildTree.addChild(node, new StringPermissionNodeChildTree(status));
            }
        }
    }

    public PermissionNodeStatus getStatus(String permission) {
        StringPermissionNodeChildTree nodeChildTree = this;
        for (String node : permission.split("\\.")) {
            nodeChildTree = nodeChildTree.getChild(node);
        }
        return nodeChildTree.status;
    }

    @Override
    public StringPermissionNodeChildTree addChild(String key, StringPermissionNodeChildTree value) {
        if (key.equals("*")) {
            value = value.status == PermissionNodeStatus.ALLOW
                    ? FixedStringPermissionNodeChildTree.ALLOW
                    : FixedStringPermissionNodeChildTree.DENY;
        }
        return super.addChild(key, value);
    }

    @Override
    public StringPermissionNodeChildTree getChild(String key) {
        return Optional.ofNullable(super.getChild("*"))
                .orElse(Optional.ofNullable(super.getChild(key))
                        .orElse(FixedStringPermissionNodeChildTree.MISSING));
    }

}
