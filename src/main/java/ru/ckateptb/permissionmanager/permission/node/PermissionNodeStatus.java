package ru.ckateptb.permissionmanager.permission.node;

public enum PermissionNodeStatus {
    ALLOW,
    DENY,
    MISSING,
    ROOT
}
