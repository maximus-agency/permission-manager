package ru.ckateptb.permissionmanager.permission.node;

public class FixedStringPermissionNodeChildTree extends StringPermissionNodeChildTree {
    public static final FixedStringPermissionNodeChildTree ALLOW = new FixedStringPermissionNodeChildTree(PermissionNodeStatus.ALLOW);
    public static final FixedStringPermissionNodeChildTree DENY = new FixedStringPermissionNodeChildTree(PermissionNodeStatus.DENY);
    public static final FixedStringPermissionNodeChildTree MISSING = new FixedStringPermissionNodeChildTree(PermissionNodeStatus.MISSING);

    private FixedStringPermissionNodeChildTree(PermissionNodeStatus isPresent) {
        super(isPresent);
    }

    @Override
    public StringPermissionNodeChildTree getChild(String key) {
        return this;
    }

    @Override
    public StringPermissionNodeChildTree addChild(String key, StringPermissionNodeChildTree value) {
        return this;
    }
}
