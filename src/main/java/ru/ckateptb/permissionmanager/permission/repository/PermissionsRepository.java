package ru.ckateptb.permissionmanager.permission.repository;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.stereotype.Repository;
import ru.ckateptb.lazystorage.storage.text.json.JsonStorage;
import ru.ckateptb.permissionmanager.permission.holder.group.GroupPermissionHolderInfo;
import ru.ckateptb.permissionmanager.permission.holder.key.KeyPermissionHolderInfo;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Getter
@Repository
@EqualsAndHashCode(callSuper = true)
public class PermissionsRepository extends JsonStorage {
    private Map<String, GroupPermissionHolderInfo> groups = new HashMap<>();
    private Map<String, KeyPermissionHolderInfo> players = new HashMap<>();

    @Override
    public void save() {
        if (!groups.containsKey("default")) {
            Map<String, GroupPermissionHolderInfo> groups = new HashMap<>();
            Set<String> defaultPermissions = new HashSet<>();
            defaultPermissions.add("example.permission.first");
            defaultPermissions.add("example.permission.second");
            defaultPermissions.add("!example.permission.denied");
            defaultPermissions.add("example.permission.all.allowed.in.this.root.*");
            groups.put("default", new GroupPermissionHolderInfo(defaultPermissions, "[default prefix]", "[default suffix]"));
            this.groups = groups;
        }
        super.save();
    }

    @Override
    public Path file() {
        return Paths.get("config", "permissions.json");
    }

}
