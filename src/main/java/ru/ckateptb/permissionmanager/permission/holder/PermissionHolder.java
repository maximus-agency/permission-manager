package ru.ckateptb.permissionmanager.permission.holder;

public interface PermissionHolder {
    boolean hasPermission(String permission);

    String getPrefix();

    String getSuffix();
}
