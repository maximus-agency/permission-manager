package ru.ckateptb.permissionmanager.permission.holder.group;

import ru.ckateptb.permissionmanager.permission.holder.AbstractPermissionHolder;
import ru.ckateptb.permissionmanager.permission.node.StringPermissionNodeChildTree;

public class GroupPermissionHolder extends AbstractPermissionHolder {
    GroupPermissionHolder(StringPermissionNodeChildTree permissions, String prefix, String suffix) {
        super(permissions, prefix, suffix);
    }
}
