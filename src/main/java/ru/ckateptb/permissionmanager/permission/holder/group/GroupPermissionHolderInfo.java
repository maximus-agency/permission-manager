package ru.ckateptb.permissionmanager.permission.holder.group;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.ckateptb.permissionmanager.permission.holder.PermissionHolderInfo;
import ru.ckateptb.permissionmanager.permission.node.PermissionNodeStatus;
import ru.ckateptb.permissionmanager.permission.node.StringPermissionNodeChildTree;
import ru.ckateptb.permissionmanager.permission.repository.PermissionsRepository;

import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GroupPermissionHolderInfo implements PermissionHolderInfo {
    private Set<String> permissions;
    private String prefix;
    private String suffix;

    public GroupPermissionHolder toHolder(PermissionsRepository repository) {
        StringPermissionNodeChildTree permissions = new StringPermissionNodeChildTree(PermissionNodeStatus.ROOT);
        permissions.addPermissions(this.permissions.toArray(new String[0]));
        return new GroupPermissionHolder(permissions, prefix, suffix);
    }
}
