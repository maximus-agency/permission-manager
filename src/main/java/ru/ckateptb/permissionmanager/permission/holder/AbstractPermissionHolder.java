package ru.ckateptb.permissionmanager.permission.holder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.ckateptb.permissionmanager.permission.node.PermissionNodeStatus;
import ru.ckateptb.permissionmanager.permission.node.StringPermissionNodeChildTree;

@Getter
@AllArgsConstructor
public abstract class AbstractPermissionHolder implements PermissionHolder {
    protected final StringPermissionNodeChildTree permissions;

    private String prefix;
    private String suffix;

    @Override
    public boolean hasPermission(String permission) {
        return permissions.getStatus(permission) == PermissionNodeStatus.ALLOW;
    }


}
