package ru.ckateptb.permissionmanager.permission.holder;

import ru.ckateptb.permissionmanager.permission.repository.PermissionsRepository;

public interface PermissionHolderInfo {
    PermissionHolder toHolder(PermissionsRepository repository);
}
