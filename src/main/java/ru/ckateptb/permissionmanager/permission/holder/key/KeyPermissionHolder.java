package ru.ckateptb.permissionmanager.permission.holder.key;

import lombok.Getter;
import ru.ckateptb.permissionmanager.permission.holder.AbstractPermissionHolder;
import ru.ckateptb.permissionmanager.permission.holder.group.GroupPermissionHolder;
import ru.ckateptb.permissionmanager.permission.node.PermissionNodeStatus;
import ru.ckateptb.permissionmanager.permission.node.StringPermissionNodeChildTree;

import java.util.Set;
import java.util.stream.Stream;

@Getter
public class KeyPermissionHolder extends AbstractPermissionHolder {
    private final Set<GroupPermissionHolder> groups;

    KeyPermissionHolder(StringPermissionNodeChildTree permissions, String prefix, String suffix, Set<GroupPermissionHolder> groups) {
        super(permissions, prefix, suffix);
        this.groups = groups;
    }

    @Override
    public String getSuffix() {
        return super.getSuffix();
    }

    @Override
    public String getPrefix() {
        return super.getPrefix();
    }


    @Override
    public boolean hasPermission(String permission) {
        switch (permissions.getStatus(permission)) {
            case DENY:
                return false;
            case ALLOW:
                return true;
            default: {
                Stream<GroupPermissionHolder> stream = groups.stream();
                return stream.noneMatch(group -> group.getPermissions().getStatus(permission) == PermissionNodeStatus.DENY) && stream.anyMatch(group -> hasPermission(permission));
            }
        }
    }
}
