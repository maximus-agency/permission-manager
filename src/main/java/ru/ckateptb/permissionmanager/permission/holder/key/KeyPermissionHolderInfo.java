package ru.ckateptb.permissionmanager.permission.holder.key;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.ckateptb.permissionmanager.permission.holder.PermissionHolderInfo;
import ru.ckateptb.permissionmanager.permission.holder.group.GroupPermissionHolder;
import ru.ckateptb.permissionmanager.permission.holder.group.GroupPermissionHolderInfo;
import ru.ckateptb.permissionmanager.permission.node.PermissionNodeStatus;
import ru.ckateptb.permissionmanager.permission.node.StringPermissionNodeChildTree;
import ru.ckateptb.permissionmanager.permission.repository.PermissionsRepository;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KeyPermissionHolderInfo implements PermissionHolderInfo {
    private String name;
    private Set<String> permissions;
    private Set<String> groups;
    private String prefix;
    private String suffix;

    @Override
    public KeyPermissionHolder toHolder(PermissionsRepository repository) {
        if (groups.isEmpty()) groups.add("default");
        Map<String, GroupPermissionHolderInfo> map = repository.getGroups();
        Set<GroupPermissionHolder> groups = this.groups.stream().map(map::get).filter(Objects::nonNull).map(info -> info.toHolder(repository)).collect(Collectors.toSet());
        StringPermissionNodeChildTree node = new StringPermissionNodeChildTree(PermissionNodeStatus.ROOT);
        node.addPermissions(this.permissions.toArray(new String[0]));
        return new KeyPermissionHolder(node, prefix, suffix, groups);
    }
}
