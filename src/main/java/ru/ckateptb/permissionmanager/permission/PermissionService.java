package ru.ckateptb.permissionmanager.permission;

import lombok.Getter;
import org.springframework.stereotype.Service;
import ru.ckateptb.permissionmanager.permission.holder.group.GroupPermissionHolder;
import ru.ckateptb.permissionmanager.permission.holder.key.KeyPermissionHolder;
import ru.ckateptb.permissionmanager.permission.repository.PermissionsRepository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class PermissionService {
    @Getter
    private final PermissionsRepository repository;
    private final Map<String, GroupPermissionHolder> groups = new HashMap<>();
    private final Map<String, KeyPermissionHolder> players = new HashMap<>();

    public PermissionService(PermissionsRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    public void update() {
        groups.clear();
        players.clear();
        repository.getGroups().forEach((name, info) -> groups.put(name, info.toHolder(repository)));
        repository.getPlayers().forEach((name, info) -> players.put(name, info.toHolder(repository)));
    }

    public boolean hasPermission(String key, String permission) {
        if (players.containsKey(key)) {
            return players.get(key).hasPermission(permission);
        }
        return groups.get("default").hasPermission(permission);
    }

    public void reload() {
        load();
        save();
    }

    public void save() {
        repository.save();
    }

    public void load() {
        repository.load();
    }
}
