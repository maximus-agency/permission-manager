package ru.ckateptb.permissionmanager.tree;

public interface ChildTree<K, V> {
    V addChild(K key, V value);

    V getChild(K key);
}
