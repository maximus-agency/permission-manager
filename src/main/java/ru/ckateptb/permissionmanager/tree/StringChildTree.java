package ru.ckateptb.permissionmanager.tree;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class StringChildTree<V> implements ChildTree<String, V> {
    private final Map<String, V> childMap = new HashMap<>();

    @Override
    public V addChild(String key, V value) {
        return Optional.ofNullable(childMap.putIfAbsent(key, value)).orElse(childMap.get(key));
    }

    @Override
    public V getChild(String key) {
        return childMap.get(key);
    }
}
